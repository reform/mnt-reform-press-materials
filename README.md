# MNT Reform Press Materials (2021 Q1)

MNT Research GmbH public media/press material.

All material except the MNT brand symbol itself is licensed CC-BY-SA 4.0 if not specified otherwise.

MNT Research GmbH CC-BY-SA 4.0

The MNT brand symbol is a registered trademark (DPMA 30 2020 215 414) of MNT Research GmbH. All rights reserved.

All material is copyright 2020-2021 MNT Research GmbH.

The following renders are copyright 2020 Paul Klingberg and require attribution: 

Paul Klingberg CC-BY-SA 4.0

- Reform_Keyboard_front.jpg
- Reform_Keyboard_top.jpg
- Reform_Beauty_CAS2.jpg
- Reform_Beauty_exploded.jpg
- Reform_Bottom2_closed.jpg
- Reform_Bottom_opened.jpg
- Reform_Beauty_exploded.jpg
- Reform_Top.jpg
- Reform_Top_closed.jpg

Press contact: sosat@mntre.com

